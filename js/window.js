//windowの高さを取得して、ページを画面いっぱいにする
function setHeight(){
  // 画面の高さを取得
  let vh = window.innerHeight;
  let vhH = vh / 5; // ヘッダーの高さ
  let vhC = vh - vhH; // コンテンツの高さ

  // ヘッダーの高さ
  var topWrapperHeader = document.getElementById('top-wrapper__header');
  topWrapperHeader.style.setProperty('height', `${vhH}px`);
  
  // index.php の場合
  // ファイル名取得
  var str = window.location.href.split('/').pop();
  var fileName = str.split('.')[0];
  if (fileName == 'indexa') {
    var topWrapper = document.getElementById('top-wrapper');
    var topWrapperContent = document.getElementById('top-wrapper__content');
    topWrapper.style.setProperty('height', `${vh}px`);
    topWrapperContent.style.setProperty('height', `${vhC}px`);
  }

}

window.addEventListener('load', setHeight);
window.addEventListener('resize', setHeight);