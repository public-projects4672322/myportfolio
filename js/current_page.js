// ファイル名を取得して、class名fileNameの要素に挿入
function currentPage(){
  var fileName = window.location.href.split('/').pop().split('.')[0];// ファイル名を取得
  var className = "now";
  if(fileName == 'index'){
    fileName = 'top';
  }
  document.getElementById(fileName).setAttribute('class', className);// ファイル名と一致するidのエレメントにclassを追加
  if(fileName == 'top') return;

  // 一文字目を大文字に変換してタイトルに挿入
  fchar = fileName.slice(0, 1);
  fchar = fchar.toUpperCase();
  fileName = fchar + fileName.slice(1);
  pageTitle = document.getElementsByClassName("fileName");
  for (let i = 0; i < pageTitle.length; i++) {
    pageTitle[i].innerText = fileName;
  }
}

window.addEventListener('load', currentPage);