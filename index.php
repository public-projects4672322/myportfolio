<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="./css/destyle.css">
  <link rel="stylesheet" href="./css/style.css">
  <title>Ken Aono's Portfolio</title>
</head>
<body>
  <?php include "./parts/header.php"; ?>
  <!-- ヘッダー 見開き1ページ -->
  <!-- <div class="top-wrapper" id="top-wrapper">
    <div class="top-wrapper__content" id="top-wrapper__content">
      <h2>Works</h2>
      <h2>Profile</h2>
      <h2>Contact</h2>
    </div>
  </div> -->

  <!-- メイン -->
  <div class="main-wrapper">
    <!-- Works -->
    <h2 class="main-wrapper__title" id="works">Works</h2>
    <section class="main-wrapper__content">
      <?php include "./parts/works.php"; ?>
    </section>

    <!-- Profile -->
    <h2 class="main-wrapper__title" id="profile">Profile</h2>
    <section class="main-wrapper__content">
      <?php include "./parts/profile.php"; ?>
    </section>

    <!-- Contact -->
    <h2 class="main-wrapper__title" id="contact">Contact</h2>
    <section class="main-wrapper__content">
      <?php include "./parts/contact.php"; ?>
    </section>


  </div><!-- /.main-wrapper -->

  <section></section>

  <script src="./js/window.js"></script>
  <script src="./js/func.js"></script>
</body>
</html>