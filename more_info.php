<?php
$id = htmlspecialchars($_GET['id']);
if (empty($id)) {
  header('Location: ./works.php');
  exit;
}
try{
  $dbname='aoken_myportfolio';
  $dbuser='aoken_myportfolio';
  $dbpass='25867439';
  $pdo = new PDO(
      'mysql:host=localhost;dbname='. $dbname .';charset=utf8',
      $dbuser,
      $dbpass
  );
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
}catch(PDOException $Exception){
  die('接続エラー：' .$Exception->getMessage());
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title></title>
  <script src="./js/window.js"></script>
  <script src="./js/func.js"></script>
  <link rel="stylesheet" href="./css/destyle.css">
  <link rel="stylesheet" href="./css/style.css">
</head>
<body>
<!-- header -->
<?php include "./parts/header.php" ?>

<!-- main -->
<!-- メイン -->
<div class="main-wrapper">
  <!-- ページ全体のタイトル -->
  <h1 class="main-wrapper__title">More Info</h1>
  <!-- ページの内容を表示するエリア -->
  <section class="main-wrapper__content">
    <div class="more-info-display-area">    
        <?php
      try{
        // SQL文の組み立て
        $sql = "SELECT s.name,sd.video_path,sd.icon_path,sd.file_path,sd.os,sd.about,sd.guide FROM works_software AS s INNER JOIN works_software_detail AS sd ON s.id = sd.id WHERE s.id = '$id'";
        // プリペアドステートメントの作成
        $stmh = $pdo->prepare($sql);
        // クエリの実行
        $stmh->execute();
      }catch(PDOException $e){
        die('接続エラー：' .$e->getMessage());
      }
      $row = $stmh->fetch(PDO::FETCH_ASSOC);
      ?>
      <h2 class="more-info-display-area__title"><?php echo $row['name'] ?></h2>
      <div class="more-info-display-area__top">
        <video src="./software/video/<?php echo $row['video_path'] ?>" type="video/mp4" autoplay muted controls></video>
        <div class="more-info-display-area__top__right">
          <img src="./software/img/<?php echo $row['icon_path'] ?>" alt="アプリアイコン">
          <a href="./software/<?php echo $row['file_path'] ?>" download>
            <h3>Download</h3>
            <img src="./img/download.svg">
            <p>
              <?php
              $os_fullname = ["win" => "Windows", "mac" => "Mac", "linux" => "Linux"];
              $os_list = explode(",", $row['os']);
              echo "For ";
              $comma = false;
              foreach ($os_list as $os) {
                if (array_key_exists($os, $os_fullname)) {
                  if ($comma) {
                    echo ", ";
                  } else {
                    $comma = true;
                  }
                  echo $os_fullname[$os];
                }
              }
              ?>
            </p>
          </a>
        </div>
      </div>
      <div class="more-info-display-area__bottom">
        <div>
          <h3 class="more-info-display-area__bottom__title">About</h3>
          <?php echo $row['about'] ?>
        </div>
        <div>
          <h3  class="more-info-display-area__bottom__title">Guide</h3>
          <ol>
            <?php echo $row['guide'] ?>
          </ol>
        </div>
      </div>
      <?php
      echo <<< EOM
      
      EOM;
      ?>
  
    </div><!-- ./more-info-display-area -->
  </section>
</div>
<!-- footer -->
<?php include "./parts/footer.php" ?>
</body>
</html>