<?php
header("Location: ./works.php");

?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>Ken Aono's Portfolio</title>
  <script src="./js/window.js"></script>
  <script src="./js/current_page.js"></script>
  <script src="./js/func.js"></script>
  <link rel="stylesheet" href="./css/destyle.css">
  <link rel="stylesheet" href="./css/style.css">
</head>
<body>
  <div class="layout">
    <!-- header -->
    <?php include "./parts/header.php"; ?>
      
    <!-- main -->
    <div class="layout__main">
      <p>a</p>
    </div>
  </div>
</body>
</html>