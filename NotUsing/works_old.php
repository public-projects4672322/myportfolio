<?php
try{
  $dbname='aoken_myportfolio';
  $dbuser='aoken_myportfolio';
  $dbpass='25867439';
  $pdo = new PDO(
      'mysql:host=localhost;dbname='. $dbname .';charset=utf8',
      $dbuser,
      $dbpass
  );
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
}catch(PDOException $Exception){
  die('接続エラー：' .$Exception->getMessage());
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>Ken Aono's Works</title>
  <link rel="stylesheet" href="./css/destyle.css">
  <script src="./js/window.js"></script>
  <script src="./js/current_page.js"></script>
  <script src="./js/func.js"></script>
  <link rel="stylesheet" href="./css/style.css">
</head>
<body>
  <div class="layout">
    <!-- header -->
    <?php require("./parts/header.php"); ?>

    <!-- main -->
    <div class="layout__main">
      <!-- ファイル名からタイトル表示 -->
      <div class="layout__main__area">
        <!-- ページ全体のタイトル -->
        <h1 class="fileName page-title">Works</h1>
        <!-- ページの内容を表示するエリア -->
        <div class="works-display-area">


          <!---------------------------------------------------
            Websites
          ---------------------------------------------------->
          <div class="works-display-area__genres">
            <?php
            $category = "Website";
            echo "<h2 class='works-category'>$category</h2>";
            ?>
            <div class="works-flex">
              <?php
              try{
                // SQL文の組み立て
                $sql = "SELECT * FROM works_website";
                // プリペアドステートメントの作成
                $stmh = $pdo->prepare($sql);
                // クエリの実行
                $stmh->execute();
              }catch(PDOException $e){
                  die('接続エラー：' .$e->getMessage());
              }
              while($row = $stmh->fetch(PDO::FETCH_ASSOC)){
                $url = './websites/' . $row['site_path'];
                echo <<< EOM
                <div class="works-wrap" onclick="DivClick('$url')">
                  <div class="works-img">
                    <div class="img">
                      <img src="./img/website-thumb/{$row['img_path']}">
                    </div>
                  </div>
                  <div class="article">
                    {$row['article']}
                  </div>
                  <div class="caption">
                    <div class="created_at">
                      {$row['created_at']}
                    </div>
                    <div class="language">
                      言語:{$row['language']}
                    </div>
                  </div>
                  <div class="hover-text"><p>Jump to Website</p></div>
                </div>
                EOM;
              }
              ?>
            </div><!-- ./works-flex -->
          </div><!-- ./works-display-area__genres -->



          <!---------------------------------------------------
            Videos
          ---------------------------------------------------->
          <div class="works-display-area__genres">
            <?php
            $category = "Video";
            echo "<h2 class='works-category'>$category</h2>";
            ?>
            <div class="works-flex">
              <?php
              try{
                // SQL文の組み立て
                $sql = "SELECT * FROM works_video";
                // プリペアドステートメントの作成
                $stmh = $pdo->prepare($sql);
                // クエリの実行
                $stmh->execute();
              }catch(PDOException $e){
                  die('接続エラー：' .$e->getMessage());
              }
              while($row = $stmh->fetch(PDO::FETCH_ASSOC)){
                echo <<< EOM
                <div class="works-wrap"">
                  <div class="works-iframe">
                    {$row['path']}
                  </div>
                  <div class="article">
                    {$row['article']}
                  </div>
                  <div class="caption">
                    <div class="created_at">
                      {$row['created_at']}
                    </div>
                    <div class="language">
                      ソフト:{$row['language']}
                    </div>
                  </div>
                </div>
                EOM;
              }
              ?>
            </div><!-- ./works-flex -->
          </div><!-- ./works-display-area__genres -->



          <!---------------------------------------------------
            SoftWare
          ---------------------------------------------------->
          <div class="works-display-area__genres">
            <?php
            $category = "Software";
            echo "<h2 class='works-category'>$category</h2>";
            ?>
            <div class="works-flex">
              <?php
              try{
                // SQL文の組み立て
                $sql = "SELECT * FROM works_software";
                // プリペアドステートメントの作成
                $stmh = $pdo->prepare($sql);
                // クエリの実行
                $stmh->execute();
              }catch(PDOException $e){
                  die('接続エラー：' .$e->getMessage());
              }
              while($row = $stmh->fetch(PDO::FETCH_ASSOC)){
                echo <<< EOM
                <div class="works-wrap" onclick="moreInfo({$row['id']})">
                  <div class="works-img">
                    <div class="img">
                      <img src="./img/software-thumb/{$row['img_path']}">
                    </div>
                  </div>
                  <div class="article">
                    {$row['article']}
                  </div>
                  <div class="caption">
                    <div class="created_at">
                      {$row['created_at']}
                    </div>
                    <div class="language">
                      言語:{$row['language']}
                    </div>
                  </div>
                  <div class="hover-text"><p>More Info</p></div>
                </div>
                EOM;
              }
              ?>
            </div><!-- ./works-flex -->
          </div><!-- ./works-display-area__genres -->


        </div><!-- ./works-display-area -->
      </div><!-- ./layout__main__area -->
    </div><!-- ./layout__main -->
  </div><!-- ./layout -->
</body>
</html>