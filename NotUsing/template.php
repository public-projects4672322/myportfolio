<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <script src="./js/window.js"></script>
  <script src="./js/current_page.js"></script>
  <script src="./js/func.js"></script>
  <link rel="stylesheet" href="./css/destyle.css">
  <link rel="stylesheet" href="./css/style.css">
  <title>template</title>
</head>
<body>
  <div class="layout">
    <!-- header -->
    <?php include "./parts/header.php" ?>
  
    <!-- main -->
    <main>
  
    </main>
  
    <!-- footer -->
    <?php include "./parts/footer.php" ?>
  </div>
</body>
</html>