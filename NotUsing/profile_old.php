<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>Ken Aono's Profile</title>
  <script src="./js/window.js"></script>
  <script src="./js/current_page.js"></script>
  <script src="./js/func.js"></script>
  <link rel="stylesheet" href="./css/destyle.css">
  <link rel="stylesheet" href="./css/style.css">
</head>
<body>
  <div class="layout">
    <!-- header -->
    <?php include "./parts/header.php"; ?>

    <?php
    $birthday = [2001, 9, 28];
    $today = explode("-", date("Y-m-d"));
    $age = $today[0] - (int)$birthday[0] - ($today[1] < (int)$birthday[1] || $today[1] == (int)$birthday[1] && $today[2] < (int)$birthday[2] ? 1 : 0);
    ?>
    

    <!-- main -->
    <div class="layout__main">
      <div class="layout__main__area">
        <!-- ファイル名からタイトル表示 -->
        <h1 class="fileName page-title"></h1>
        <div class="profile-display-area">
          <div class="profile-display-area__content">
            <div class="profile-display-area__content">
              <div class="profile-display-area__content__left">
                <img src="./img/profile.png">
              </div>
              <div class="profile-display-area__content__right">
                <table>
                  <tr>
                    <th>Name</th>
                    <td>Ken Aono</td>
                  </tr>
                  <tr>
                    <th>Age</th>
                    <td><?php echo $age; ?></td>
                  </tr>
                  <tr>
                    <th>MBTI</th>
                    <td>INTP</td>
                  </tr>
                  <tr>
                    <th>Job</th>
                    <td>大学生</td>
                  </tr>
                  <tr>
                    <th>Hobbies</th>
                    <td>ゲーム, 料理, 動画制作</td>
                  </tr>
                  <tr>
                    <th>Favorite food</th>
                    <td>麻婆豆腐, チャーハン, 寿司, 焼肉</td>
                  </tr>
                </table>
              </div><!-- ./profile-display-area__content__right -->
            </div><!-- ./profile-display-area__content__left -->
          </div><!-- ./profile-display-area__content -->
          <!-- <div class="profile-display-area__content2">
            <div class="profile-display-area__content2__main">
              <div class="profile-display-area__content2__main__first">
                <div></div>
              </div>
              <div class="profile-display-area__content2__main__second">
                <div></div>
              </div>
              <div class="profile-display-area__content2__main__third">
                <div></div>
              </div>
            </div>
          </div>./profile-display-area__content2 -->
        </div><!-- ./profile-display-area -->
      </div><!-- ./layout__main__area -->
    </div><!-- ./layout__main -->
</div>
</body>
</html>