// JavaScript
// sleep
async function sleep(ms) {
  let wait_promise = new Promise( resolve => { setTimeout( resolve, ms ) } );
  await wait_promise;
}

// 春夏秋冬ページ ボタンクラス付与削除
window.onload = async function buttonClassChange() {
  // await sleep(500);
  await sleep(3000);
  var buttons = document.getElementsByClassName("fadeUp");
  var loop = buttons.length;
  var j = 0;
  for (let i = 0; i < loop; i++) {
    buttons[j].classList.add("button_hop");
    buttons[j].classList.remove("fadeUp");
  }
}


// jQuery
// スクロールヘッダー固定
function headerFixed() {
  var height = $(".top-main").outerHeight(true) + 430;
  var scroll = $(window).scrollTop();
  if(scroll >= height) {
    $(".header-fixed__wrapper").removeClass("display-none");
    $("#page-top__button").removeClass("display-none");
  }else{
    $(".header-fixed__wrapper").addClass("display-none");
    $("#page-top__button").addClass("display-none");
  }
}
// 画面をスクロールをしたら動かす
$(window).scroll(function () {
	headerFixed();
});
$(function(){
  headerFixed();
});


//  スクロールフェードイン
$(function () {
  $(window).scroll(function () {
    const windowHeight = $(window).height();
    const scroll = $(window).scrollTop();

    $('.wrapper-wide__content__sub , .top-page__content').each(function () {
      const targetPosition = $(this).offset().top;
      if (scroll > targetPosition - windowHeight + 150) {
        $(this).addClass("fadeIn");
      }
    });
  });
});



// ページ一枚分スクロール
function pageScroll() {
  let wh = window.innerHeight;
  scrollTo(0, wh);
}