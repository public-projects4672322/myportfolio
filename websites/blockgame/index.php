<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="./css/destyle.css">
    <link rel="stylesheet" href="./css/style.css">
  <title>テトリスっぽいゲーム</title>
</head>
<body>
  <div class="wrapper">

    <!-- テトリスの点などを表示 -->
    <div id="point-display-area">
      <div id="point-display">
        <div id="turn-area" class="point-display__element">
          <h2 id="turn-title">TURN</h2>
          <p id="turn">0</p>
        </div>
        <div id="difficulty-area" class="point-display__element">
          <h2 id="difficulty-title">LEVEL</h2>
          <p id="difficulty">&#8544</p>
        </div>
        <div id="score-area" class="point-display__element">
          <h2 id="score-title">SCORE</h3>
          <p id="score">0</p>
        </div>
      </div>
    </div>
    

    <!-- プレイエリア -->
    <div id="area">
      <p id="message" class="display-none">GAME OVER</p>
    </div>
    
    
    <!-- 右側の表示エリア -->
    <div id="right-display-area">
      <!-- <button onclick="placeBlock()" id="start">START</button> -->
      <!-- <button id="start" onclick="gameExecute()">START</button> -->
      <div id="right-display">
        <div class="right-display__element" id="next-block-element">
          <h4>Next Block</h4>
          <table id="next-block">
            <?php
            for ($i=0; $i < 3; $i++) { 
              echo("<tr>");
              for ($j=0; $j < 4; $j++) {
                echo("<td id='nb-{$i},{$j}'></td>");
              }
              echo("</tr>");
            }
            ?>
          </table>
        </div>
        <div class="right-display__element" id="points-table">
          <h4>Points Table</h4>
            <ul>
              <?php
              $point = [0, 100, 300, 600, 1000];
              for ($i = 1; $i <= 4; $i++){
                echo "<li>{$i}列消し: $point[$i] points</li>";
              }
              ?>
            </ul>
        </div>
        <div class="right-display__element" id="start-stop-buttons">
          <h4>Buttons</h4>
          <div class="start-stop">
            <img id="restart" src="img/start.png" class="display-none">
            <img id="stop" src="img/stop.png">
          </div>
          <img id="reset" src="./img/reload.svg">
        </div>
      </div>
    </div>


    <!-- ホップアップ画面 -->
    <div class="hopup-wrapper" id="menu-wrapper-full">
      <div class="menu-wrapper" id="menu-wrapper">
        <button id="start" onclick="gameExecute()">START</button>
        <dl id="score-rank">
          <h3>SCORE</h3>
          <dt>1 .</dt><dd id="rank1st">0</dd>
          <dt>2 .</dt><dd id="rank2nd">0</dd>
          <dt>3 .</dt><dd id="rank3rd">0</dd>
        </dl>
      </div>
    </div>


  </div>
  <script src="./js/func.js"></script>
  <script src="./js/rotate.js"></script>
  <script src="./js/script.js"></script>
  <script src="./js/exe.js"></script>
  <script src="./js/style.js"></script>
</body>
</html>