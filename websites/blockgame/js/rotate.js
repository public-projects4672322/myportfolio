/************************************************************
 ブロックを回転させる関数
************************************************************/
function rotateBlocks(ary_xy, color, active){
  if(active == false) return;
  let mid = new Array();
  let rotate = new Array();
  let ary_copy = JSON.parse(JSON.stringify(ary_xy)); // ary_xyのディープコピー
  let i, j;
  let x, y;
  let row, col;
  let el;
  let move = 0;
  switch (color) {
    case 'yellow': // O 四角ブロックの時は何もしない
    break;
    case 'aqua': // I 棒ブロックの回転
      let r = 0, c = 0, m = 0, n = 0;
      mid = ary_xy[2].concat();// 回転する中心をmidに格納
      if(ary_xy[1][0] == ary_xy[2][0]){// 棒が横のとき
        if(ary_xy[1][1] < ary_xy[2][1]){// 向きが左から右のとき
          r = -1;
          m = 1;
        }else{ // 向きが右から左のとき
          r = 1;
          m = -1;
        }
      }else{ // 棒が縦のとき
        if(ary_xy[1][0] < ary_xy[2][0]){ // 向きが上から下のとき
          c = 1;
          n = -1;
        }else{ // 向きが下から上のとき
          c = -1;
          n = 1;
        }
      }
      for(i = 0; i < ary_xy.length; i++){// 回転させる
        row = mid[0] + r;
        col = mid[1] + c;
        if(row < 0 || row > 19) return ary_copy;
        if(col < 0) {
          if(n == -1){
            move = 2;
          }else{
            move = 1;
          }
        }else if(col > 9) {
          if(n == -1){
            move = -1;
          }else{
            move = -2;
          }
        }
        ary_xy[i] = [row, col];
        r += m;
        c += n;
      }
      for(i = 0; i < ary_xy.length; i++){ // その先にすでにブロックがないか判定
        ary_xy[i][1] += move; // 移動させる
        el = document.getElementById(ary_xy[i][0]+','+ary_xy[i][1]); // ブロックのid
        if(el.classList.contains('inactive')) return ary_copy; // ブロックがあった場合,ary_copyを返す
      }
      for(i = 0; i < ary_xy.length; i++){// 現在地のブロックを全削除
        deleteBlock(ary_copy[i][0], ary_copy[i][1], color);
      }
      for(i = 0; i < ary_xy.length; i++){// 移動先にブロックを表示
        appearBlock(ary_xy[i][0], ary_xy[i][1], color);
      }
      break;

    default: // 四角or棒ブロック以外の回転
      if(color == 'green'){
        mid = ary_xy[3]
      }else{
        mid = ary_xy[2];// ブロックの中心をmidに格納
      }
      for(i = 0; i < ary_xy.length; i++){// ブロックの中心をもとにブロックの相対的な座標を計算し配列rotateに格納
        x = Math.abs(ary_xy[i][1] - mid[1]); // 列からx座標を計算
        y = Math.abs(ary_xy[i][0] - mid[0]); // 行からy座標を計算
        if(ary_xy[i][1] < mid[1]) x *= -1; // 座標がマイナスの時-1をかける
        if(ary_xy[i][0] > mid[0]) y *= -1; // 上に同じ
        if(x**2 + y**2 != 1){
          x /= Math.sqrt(2);
          y /= Math.sqrt(2);
        }
        rotate.push([x, y]); // rotateに格納
      }
      for (i = 0; i < ary_xy.length; i++) { // 極座標変換してそれぞれ90度時計周りに動かす
        if(!(rotate[i][0] == 0 && rotate[i][1] == 0)){// 中心は動かさない
          y = Math.asin(rotate[i][1]); // yについて極座標変換
          if(rotate[i][0] < 0){ // yの値をもとに角度を調整
            y = Math.PI - y;
          }
          x = Math.acos(rotate[i][0]); // xについて極座標変換
          if(rotate[i][1] < 0){ // xの値をもとに角度を調整
            x *= -1;
          }
          x -= (Math.PI / 2); // -90°する
          y -= (Math.PI / 2);
          x = Math.round(Math.cos(x)); // 座標を戻す
          y = Math.round(Math.sin(y));
          rotate[i] = [x, y]; // rotateに格納
        }
      }
      // return ary_copy;
      for(i = 0; i < ary_xy.length; i++){ // ブロックの中心をもとにブロックの絶対的な座標を計算
        row = mid[0] - rotate[i][1]; // 行
        col = mid[1] + rotate[i][0]; // 列
        if(col < 0) move = 1; // 回転先が壁を超えた場合、ずらす
        if(col > 9) move = -1;
        if(row < 0 || row > 19) return ary_copy;// 底or天井のとき回さない
        ary_xy[i] = [row, col]; // ary_xyに格納
      }
      for(i = 0; i < ary_xy.length; i++){ // その先にすでにブロックがないか判定
        ary_xy[i][1] += move; // 移動させる
        el = document.getElementById(ary_xy[i][0]+','+ary_xy[i][1]); // ブロックのid
        if(el.classList.contains('inactive')) return ary_copy; // ブロックがあった場合,ary_copyを返す
      }
      for(i = 0; i < ary_xy.length; i++){// 現在地のブロックを全削除
        deleteBlock(ary_copy[i][0], ary_copy[i][1], color);
      }
      for(i = 0; i < ary_xy.length; i++){// 移動先にブロックを表示
        appearBlock(ary_xy[i][0], ary_xy[i][1], color);
      }
      break;
  }
  return ary_xy;
}