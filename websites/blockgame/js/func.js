/************************************************************
 配列要素を昇順または降順に並び替える関数
************************************************************/
function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

const aryMax = function (a, b) {return Math.max(a, b);}
const aryMin = function (a, b) {return Math.min(a, b);}


/************************************************************
 ブロックを消す関数 + ブロックを生成する関数
************************************************************/
function deleteBlock(row, col, color){
  document.getElementById(row + ',' + col).classList.remove(color, 'block', 'active');
}
function appearBlock(row, col, color){
  document.getElementById(row + ',' + col).classList.add(color, 'block', 'active');
}

/************************************************************
 そろったら消すための関数
************************************************************/
function downBlock(row, col) {// ラインが消されたときブロックを下にずらす関数
  let e1, color;
  e1 = document.getElementById(row + ','+ col).classList;
  e1.remove('block', 'inactive');
  for(let key in colors){
    if(e1.contains(colors[key])){
      color = colors[key];
      e1.remove(color);
      break;
    }
  }
  document.getElementById((row+1) + ','+ col).classList.add(color, 'block', 'inactive');
}
function eraseLine(row, col) {//そろっていたら一列消す関数
  let e1, color;
  e1 = document.getElementById(row + ','+ col).classList;
  e1.remove('block', 'inactive');
  for(let key in colors){
    if(e1.contains(colors[key])){
      color = colors[key];
      e1.remove(color);
      break;
    }
  }
}




/************************************************************
 スコアの大きさを調整し表示する関数
************************************************************/
const scoreAreaEle = document.getElementById('score-area'); // スコアを表示するエリアの要素
let scoreAreaEleW = scoreAreaEle.offsetWidth;
const scoreEle = document.getElementById('score'); // スコアを表示するHTMLの要素

const canvas = document.createElement('canvas');// canvas生成
const context = canvas.getContext('2d');// contextの取得
let metrics;// 計測するための変数
let width; // 文字列の幅
let text; //計測する文字列
let fontSize = 50;//初期の文字の大きさ

function han2Zen(str) { //文字幅を求めるために半角を全角に変換
  return str.replace(/[0-9]/g, function(s) {
      return String.fromCharCode(s.charCodeAt(0) + 0xFEE0);
  });
}
function zen2Han(str) { //全角数字を半角に戻す
  return str.replace(/[０-９]/g, function(s) {
      return String.fromCharCode(s.charCodeAt(0) - 0xFEE0);
  });
}

function adjustSize(str, size){//文字の大きさを調整する
  context.font = `${size}px Anton`;// 初期fontsizeの指定
  text = han2Zen(str);
  metrics = context.measureText(text);
  width = metrics.width;
  if(width < scoreAreaEleW){
    return size;
  }else{
    return adjustSize(str, size-1)
  }
}
function scoreOutput(score, size){//スコアを画面に表示する
  let fontSize;
  text = score.toLocaleString();
  fontSize = adjustSize(text, size);
  scoreEle.style.fontSize = `${fontSize}px`;
  text = zen2Han(text);
  scoreEle.innerText = text;
  return fontSize;
}


/************************************************************
  テトリス用の10×20マスの表をリセットし、再度作成
************************************************************/
//マスのリセット
function resetArea(){
  window.location.reload();
}
document.getElementById('reset').addEventListener('click', resetArea, false);




/************************************************************
  次のブロックを表示する
************************************************************/
function showNextBlock(type){
  // クラスの全初期化
  let target;
  for(let i=0; i < 3; i++){
    for(let j=0; j < 4; j++){
      target = document.getElementById(`nb-${i},${j}`);
      target.classList.remove(...target.classList);
    }
  }
  let nextBlock = new Array();
  let colors = { I:'aqua', O:'yellow', T:'purple', J:'blue', L:'orange', S:'green', Z:'red', Plus:'gray' };
  let color;
  switch (type){
    case 0: // I aqua ////////////////////////////
      for(let i = 0; i < 4; i++){
        nextBlock.push([1,i]);
      }
      color = colors['I'];
      break;
    case 1: // O yellow ////////////////////////
      nextBlock = [[1,1],[1,2],[2,1],[2,2]];
      color = colors['O'];
      break;
    case 2: // T purple //////////////////////////
      nextBlock.push([1,1]);
      for (let i = 0; i < 3; i++) {
        nextBlock.push([2,i]);
      }
      color = colors['T'];
      break;
    case 3: // J blue ////////////////////////////
      nextBlock.push([2,1]);
      for (let i = 0; i < 3; i++) {
        nextBlock.push([i,2]);
      }
      color = colors['J'];
      break;
    case 4: // L orange //////////////////////////
      nextBlock.push([2,2]);
      for (let i = 0; i < 3; i++) {
        nextBlock.push([i,1]);
      }
      color = colors['L'];
      break;
    case 5: // S green ///////////////////////////
      nextBlock = [[2, 0],[2, 1],[1, 1],[1, 2]];
      color = colors['S'];
      break;
    case 6: // Z red /////////////////////////////
      nextBlock = [[1, 0],[1, 1],[2, 1],[2, 2]];
      color = colors['Z'];
      break;
    case 7: // Plus gray //////////////////////////
      nextBlock = [[1, 1],[0, 2],[1, 2],[2, 2], [1, 3]];
      color = colors['Plus'];
      break;
  }
  for (let i = 0; i < nextBlock.length; i++) {
    document.getElementById('nb-'+nextBlock[i][0]+','+nextBlock[i][1]).classList.add(color);
  }
}





/************************************************************
  点数を保存するcookieの操作
************************************************************/
function setCookie(key, value){
  document.cookie = `${key}=${value}`;
  // document.cookie = "currentScore=100";
}
function getCookie(key){
  let value = document.cookie;
}
function deleteCookie(key){
  document.cookie = `${key}=0; max-age=0`;
}
// すべてのcookieを削除
function deleteAllCookies() {
  const cookies = document.cookie.split(';')
  for (let i = 0; i < cookies.length; i++) {
    const cookie = cookies[i]
    const eqPos = cookie.indexOf('=')
    const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie
    document.cookie = name + '=;max-age=0'
  }
}


// 初回アクセス時に1,2,3位のcookieを初期化
function initializeCookies(){
  let value = document.cookie;
  let maxAge = 60 * 60 * 24 * 7;
  if (value.indexOf('first') == -1){
    document.cookie = 'first=0; max-age=' + maxAge;
    document.cookie = 'second=0; max-age=' + maxAge;
    document.cookie = 'third=0; max-age=' + maxAge;
    document.cookie = 'currentScore=0; max-age=' + maxAge;
  }
}

// 現在のスコアをランキングに反映させる
function setScoreToRank(){
  let cookieValues = document.cookie;
  let valueArray = cookieValues.split('; ');
  let tmp, currentScore;
  let scoreRanks = {first: 0, second: 0, third: 0}
  // console.table(valueArray);
  // cookieを取り出して、分離する
  valueArray.forEach(element => {
    if(element.indexOf('first') > -1){
      tmp = element.split('=')[1];
      scoreRanks['first'] = Number(tmp);
    }else if(element.indexOf('second') > -1){
      tmp = element.split('=')[1];
      scoreRanks['second'] = Number(tmp);
    }else if(element.indexOf('third') > -1){
      tmp = element.split('=')[1];
      scoreRanks['third'] = Number(tmp);
    }else if(element.indexOf('currentScore') > -1){
      tmp = element.split('=')[1];
      currentScore = Number(tmp);
    }
  });
  // console.table(scoreRanks);
  // ランキングのスコアと比較
  if(scoreRanks['first'] < currentScore){
    scoreRanks['third'] = scoreRanks['second'];
    scoreRanks['second'] = scoreRanks['first'];
    scoreRanks['first'] = currentScore;
    setCookie('first', currentScore);
    setCookie('second', scoreRanks['second']);
    setCookie('third', scoreRanks['third']);
  }else if(scoreRanks['second'] < currentScore){
    scoreRanks['third'] = scoreRanks['second'];
    scoreRanks['second'] = currentScore;
    setCookie('second', currentScore);
    setCookie('third', scoreRanks['third']);
  }else if(scoreRanks['third'] < currentScore){
    setCookie('third', currentScore);
    scoreRanks['third'] = currentScore;
  }
  document.getElementById('rank1st').innerText = scoreRanks['first'];
  document.getElementById('rank2nd').innerText = scoreRanks['second'];
  document.getElementById('rank3rd').innerText = scoreRanks['third'];
  setCookie('currentScore', 0);
}

window.addEventListener('load', initializeCookies);
window.addEventListener('load', setScoreToRank);
// window.addEventListener('load', deleteAllCookies);
