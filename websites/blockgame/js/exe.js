/************************************************************
  フィールド生成
************************************************************/
window.addEventListener('load', makeArea);

/************************************************************
  ブロック生成
************************************************************/
let returnValues = new Array();
let returnValue = true;
let flg;
let difficulty = 1;
let difficultyByTurn = 5;

let lineCnt = 0; //消された行数を格納
let score = 0; //スコア
let turn = 1; //ターン数
const turnEle = document.getElementById('turn'); // ターン数を表示するHTMLの要素

// 出てくる形をランダムで指定
let type = new Array();
type[0] = getRandomInt(8);
type[1] = getRandomInt(8);


async function gameTurn(){ // ゲームを進行させる関数
  while(returnValue){// ゲームオーバになるまでループ
    showNextBlock(type[1]);
    returnValues = await generateShape(type[0]);// ランダムなブロックの生成
    returnValues = await placeBlock(returnValues[0], returnValues[1]);// ブロックをランダムな位置に配置
    returnValue = await dropBlocks(returnValues[0], returnValues[1], difficulty);// ブロックを移動させる
    lineCnt = 0;// 消された行カウンタの初期化
    lineCnt += await lineCheck();// ブロックが横にそろっていたら消す、消した行数を返す
    returnValue = await checkGameover();// ブロックが上まで埋まったら処理を終了する

    turnEle.innerHTML = ++turn;// ターンを表示
    score += lineCnt * 100// 行数に100をかけてスコアにする
    fontSize = scoreOutput(score, fontSize);// スコアをhtml表示

    if(turn % difficultyByTurn == 0 && difficulty < 12){
      document.getElementById('difficulty').innerHTML = '&#' + (8544+difficulty++);
    }
    // 現在のスコアをcookieに保存
    setCookie('currentScore', score);
    // nextblockを次にだし、新たにnextblockを生成
    type[0] = type[1];
    type[1] = getRandomInt(8);
  }
  if(returnValue == false) {
    setScoreToRank();
  }
}


function gameExecute(){ //スタートボタンが押されたらこれを実行する
  turnEle.innerText = turn;// 1ターン目
  document.getElementById('menu-wrapper-full').classList.add('display-none');
  gameTurn();// ゲームを実行
}




