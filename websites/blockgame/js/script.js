'use strict'
/************************************************************
  テトリス用の10×20マスの表を作成
************************************************************/
var blocks = Array.from(new Array(22), () => new Array(10).fill(0));
var area = document.getElementById('area');
var tbl = document.createElement("table");
tbl.setAttribute('id', 'playArea');
let ary_xy = new Array();

function makeArea(){
  // すべてのセルを作成
  for (var i = 0; i < blocks.length; i++) {
    // 表の行を作成
    var row = document.createElement("tr");

    for (var j = 0; j < blocks[i].length; j++) {
      var cell = document.createElement("td");
      cell.setAttribute('id', i+','+j);
      cell.setAttribute('class', '');
      row.appendChild(cell);
    }
  // 表の本体の末尾に行を追加
  tbl.appendChild(row);
  }
  area.appendChild(tbl);
}


/************************************************************
  ブロック生成
************************************************************/
let colors = { I:'aqua', O:'yellow', T:'purple', J:'blue', L:'orange', S:'green', Z:'red', Plus:'gray'};
function generateShape(type){
  return new Promise((resolve) => {
    // let type = getRandomInt(7);
    // type = 7;
    let width = blocks[0].length;
    let x;
    let color;
    ary_xy.splice(0);
    
    switch (type){
      case 0: // I aqua ////////////////////////////
        x = getRandomInt(width - 3);
        for(let i = 0; i < 4; i++){
          ary_xy.push([0,x+i]);
        }
        color = colors['I'];
        break;
      case 1: // O yellow ////////////////////////
        x = getRandomInt(width - 1);
        ary_xy = [[0,x],[0,x+1],[1,x],[1,x+1]];
        color = colors['O'];
        break;
      case 2: // T purple //////////////////////////
        x = getRandomInt(width - 2) + 1;
        // x = 5;
        ary_xy.push([0,x]);
        for (let i = -1; i <= 1; i++) {
          ary_xy.push([1,x+i]);
        }
        color = colors['T'];
        break;
      case 3: // J blue ////////////////////////////
        x = getRandomInt(width - 2);
        ary_xy.push([0,x]);
        for (let i = 0; i < 3; i++) {
          ary_xy.push([1,x+i]);
        }
        color = colors['J'];
        break;
      case 4: // L orange //////////////////////////
        x = getRandomInt(width - 2);
        ary_xy.push([0,x+2]);
        for (let i = 0; i < 3; i++) {
          ary_xy.push([1,x+i]);
        }
        color = colors['L'];
        break;
      case 5: // S green ///////////////////////////
        x = getRandomInt(width - 2) + 1;
        ary_xy = [[0, x],[0, x+1],[1, x-1],[1, x]];
        color = colors['S'];
        break;
      case 6: // Z red /////////////////////////////
        x = getRandomInt(width - 2) + 1;
        ary_xy = [[0, x-1],[0, x],[1, x],[1, x+1]];
        color = colors['Z'];
        break;
      case 7: // Plus gray //////////////////////////
        x = getRandomInt(width - 3);
        ary_xy = [[1, x],[0, x+1],[1, x+1],[2, x+1], [1, x+2]];
        color = colors['Plus'];
        break;
    }
    // placeBlock(ary_xy, color);
    // return [ary_xy, color];
    resolve([ary_xy, color])
  });
}


/************************************************************
  ブロックを配置
************************************************************/
function placeBlock(ary_xy, color) {
  return new Promise((resolve) => {
    for (let i = 0; i < ary_xy.length; i++) {
      document.getElementById(ary_xy[i][0]+','+ary_xy[i][1]).classList.add('block', color, 'active');
    }
    // dropBlock(ary_xy, color);
    // return [ary_xy, color];
    resolve([ary_xy, color]);
  });
}


/************************************************************
 ブロックの移動先判定からの移動をする関数
************************************************************/
function downBlockCheck(ary_xy, color, active){ // 下にブロックを動かせるか判定し、動かす関数
  if(active == false) return;
  var ary_y = new Array();
  let element, i, max;

  for (i = 0; i < ary_xy.length; i++) { // y座標を配列ary_yにまとめる
    ary_y.push(ary_xy[i][0])
  }
  max = ary_y.reduce(aryMax); // ブロックの一番下のy座標をmaxに格納

  for (i = 0; i < ary_xy.length; i++) { // 動かせるか判定
    if(ary_xy[i][0]+1 < blocks.length){
      element = document.getElementById((ary_xy[i][0]+1)+','+ary_xy[i][1]);
      if(element.classList.contains('inactive')){
        return 99; // 下にブロックがあるとき
      }
    }else{
      return 100; // マスの底についたとき
    }
  }
  for(i = 0; i < ary_xy.length; i++){// 現在地のブロックを全削除
    deleteBlock(ary_xy[i][0], ary_xy[i][1], color);
    ary_xy[i][0]++;
  }
  for(i = 0; i < ary_xy.length; i++){// 移動先にブロックを表示
    appearBlock(ary_xy[i][0], ary_xy[i][1], color);
  }

  return max+1; // 正常に動いた場合,ブロックの一番下の行数を返す
}


function slideBlockCheck(ary_xy, d, active, color){ //横方向に動けるか判定し、動かす
  if(active == false) {return ary_xy;}
  var ary_x = new Array();
  let element, i, side;
  for (i = 0; i < ary_xy.length; i++) {// x座標を配列ary_xにまとめる
    ary_x.push(ary_xy[i][1]);
  }
  if(d > 0){ // 移動させる方向の端をsideに代入
    side = ary_x.reduce(aryMax);
  }else{
    side = ary_x.reduce(aryMin);
  } 
  for (i = 0; i < ary_xy.length; i++) { // 動かせるか判定
    if(ary_xy[i][1] == side){
      if(ary_xy[i][1]+d < blocks[i].length && ary_xy[i][1]+d >= 0){
        element = document.getElementById(ary_xy[i][0]+','+(ary_xy[i][1]+d));
        if(element.classList.contains('block')){
          return ary_xy; // 横にブロックがあるとき
        }
      }else{
        return ary_xy; // マスの壁あたったとき
      }
    }
  }
  for(i = 0; i < ary_xy.length; i++){ // 現在地のブロックを全削除
    deleteBlock(ary_xy[i][0], ary_xy[i][1], color);
    ary_xy[i][1] += d;
  }
  for(i = 0; i < ary_xy.length; i++){ // 移動先にブロックを表示
    appearBlock(ary_xy[i][0], ary_xy[i][1], color);
  }
  return ary_xy;
}


/************************************************************
  ブロックを上から下まで落とす関数
************************************************************/
const dropBlocks = function dropBlock(ary_xy, color, difficulty = 1) {
  return new Promise((resolve) =>{
    let i = 0;
    let cnt = 0;
    let intcnt;
    let active = true;
    let interval = true;
    let fin = false;
    let cycle_time = 800 * ((13-difficulty)/12);
    let clearId = setInterval(function(){
      if(interval == false) return; 
      intcnt = Math.floor(cnt);
      if(intcnt > blocks.length-1){
        i = downBlockCheck(ary_xy, color);
        if(i >= 99){
          stopInterval();
          toInactive(ary_xy);
          ary_xy.splice(0);
          fin = true;
          resolve(fin);
          // lineCheck(); //ラインそろってるか判定
        }else{
          cnt = ary_xy[i-1][0];
        }
      }else{
        cnt = downBlockCheck(ary_xy, color); // 下にブロックがあるか、判定しなかったら移動させる
      }
      
      // cnt += 0.125;
      cnt++;
    }, cycle_time);
    function stopInterval() {
      active = false;
      clearInterval(clearId);
    }

    function restartButton(){
      if(fin) return;
      active = true;
      interval = true;
      document.getElementById('restart').classList.add('display-none');
      document.getElementById('stop').classList.remove('display-none');
    }
    function stopButton(){
      if(fin) return;
      active = false;
      interval = false;
      document.getElementById('stop').classList.add('display-none');
      document.getElementById('restart').classList.remove('display-none');
    }
    document.getElementById('restart').addEventListener('click', restartButton, false);
    document.getElementById('stop').addEventListener('click', stopButton, false);
  
    // 矢印キーが押されたとき、対象の動作をする
    function keypressEvent(e){
      if(active == false) return;
      switch (e.key) {
        case 'ArrowUp':
          ary_xy = rotateBlocks(ary_xy, color, active);
          break;
        case 'ArrowDown':
          i = downBlockCheck(ary_xy, color, active);
          break;
        case 'ArrowRight': //右に移動
          slideBlockCheck(ary_xy, 1, active, color);
          // console.table(ary_xy);
          break;
        case 'ArrowLeft': //左に移動
          slideBlockCheck(ary_xy, -1, active, color);
          // console.table(ary_xy);
          break;
      }
    }
    document.addEventListener('keydown', keypressEvent, false);
  });
}



/************************************************************
  ブロックを非アクティブにする
************************************************************/
function toInactive(ary_xy){
  let e;
  for (let i = 0; i < 4; i++) {
    e = document.getElementById(ary_xy[i][0]+','+ary_xy[i][1]);
    e.classList.remove('active');
    e.classList.add('inactive');
  }
}



/************************************************************
  そろっていたら消す
************************************************************/
function lineCheck(){
  return new Promise((resolve) => {
    let i,j,k;
    let lines = new Array();
    let lineCnt = 0;
    let e1;
    let bonus = 1;
    let stack = 1;
    for(i = 0; i < blocks.length; i++){ // 行のループ
      for(j = 0; j < blocks[i].length; j++){ // 列のループ
        e1 = document.getElementById(i+','+j); // 要素一つ取り出す
        if(!(e1.classList.contains('block'))){ // 横に見ていってブロックが空だったらbreak
          break;
        }
      }
      if(j == blocks[i].length){ //一行ブロックで埋まってるとき
        lines.push(i);// 行番号をlinesに格納
        for(j = 0; j < blocks[i].length; j++){// 行を一列消す
          eraseLine(i, j);
        }
      }
    }
    if(lines.length != 0) {// 一行でも消した行があるとき,下にスライドさせる
      for (i = 0; i < lines.length; i++) {
        for(j = lines[i]; j >= 0; j--){
          for(k = 0; k < blocks[i].length; k++){
            e1 = document.getElementById(j+','+k).classList;
            if(e1.contains('block') == true){
              downBlock(j, k);
            }
          }
        }
      }
      // if(lines.length > 1){// 2行以上消したとき、ボーナスポイントを計算
      //   for(i = 0; i < lines.length-1; i++){
      //     if(lines[i] == lines[i + 1]+1){
      //       stack++;
      //     }else{
      //       if(bonus)
      //       bonus += stack;
      //       stack = 0;
      //     }
      //   }
      // }
      lineCnt = lines.length * ((lines.length + 1)/2);
    }

    resolve(lineCnt);
    // checkGameover();
  })
}

/************************************************************
  上まで埋まったらゲーム終了
************************************************************/
function checkGameover(){
  return new Promise((resolve) => {
    let i = 0;
    let e1;
    let last = true;
    for (i = 0; i < blocks[0].length; i++){
      e1 = document.getElementById('0,'+i);
      if(e1.classList.contains('block') == true){
        last = false;
        document.getElementById('message').classList.remove('display-none');
        break;
      }
    }
    // generateShape();
    // return flg;
    resolve(last);
  });
}

