/************************************************************
  エリアの幅をレスポンシブルにする
************************************************************/
function playAreaWidth() {
  width = document.documentElement.clientWidth;
  el1 = document.getElementById('area');
  el2 = document.getElementById('playArea');
  setWidth = width / 5.5
  el1.style.width = setWidth+'px';
  el2.style.width = setWidth+'px';
}


window.addEventListener('load', playAreaWidth);
window.addEventListener('resize', playAreaWidth);