<?php
$birthday = [2001, 9, 28];
$today = explode("-", date("Y-m-d"));
$age = $today[0] - (int)$birthday[0] - ($today[1] < (int)$birthday[1] || $today[1] == (int)$birthday[1] && $today[2] < (int)$birthday[2] ? 1 : 0);
?>
<div class="profile-display-area__content">
  <table>
    <tr>
      <th>Name</th>
      <td>Ken Aono</td>
    </tr>
    <tr>
      <th>Age</th>
      <td><?php echo $age; ?></td>
    </tr>
    <tr>
      <th>MBTI</th>
      <td>INTP</td>
    </tr>
    <tr>
      <th>Job</th>
      <td>大学生</td>
    </tr>
    <tr>
      <th>Hobbies</th>
      <td>ゲーム, 料理, 動画制作</td>
    </tr>
  </table>
</div><!-- ./profile-display-area__content -->