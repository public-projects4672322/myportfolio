<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
</head>
<body>
  <!-- header -->
  <header class="layout__header">
    <a href="./index.php"><h1>Ken Aono's<br>Portfolio</h1></a>
    <ul>
      <!-- <a href="./index.php"><li id="top">TOP</li></a> -->
      <a href="./works.php"><li id="works">Works</li></a>
      <a href="./profile.php"><li id="profile">Profile</li></a>
    </ul>
  </header>
</body>
</html>